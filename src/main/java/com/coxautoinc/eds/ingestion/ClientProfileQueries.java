package com.coxautoinc.eds.ingestion;

/**
 * Created by akgillella on 7/22/16.
 */
public class ClientProfileQueries {


    public static final String QueryName_PULL_QUARTERLY_REVENUE_ALL = "pull_quarterly_revenue_all";

    public static final String PULL_QUARTERLY_REVENUE_ALL = "SELECT\n" +
            "   0 s_0,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Accounting Date\".\"Accounting Month\" s_1,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customer Account\".\"Account Number\" s_2,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customer Account\".\"Account Type Code\" s_3,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Number\" s_4,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Subtype\" s_5,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Business Unit\" s_6,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Finance LOB\" s_7,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"RMB LOB Code\" s_8,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"GL Account\".\"Segment 3 Code\" s_9,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"GL Account\".\"Segment 3 Name\" s_10,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Franchise Indicator\" s_11,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Name\" s_12,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Operational Role\" s_13,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Sales View Flag\" s_14,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Status\" s_15,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer SVOC ID\" s_16,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Type\" s_17,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Dealer Group Flag\" s_18,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Dealer Group ID Level 1\" s_19,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Dealer Group Name Level 1\" s_20,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"DMA Code\" s_21,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Primary Cust  SVOC ID\" s_22,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Region ID\" s_23,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Revenue\".\"Revenue Amount >0\" s_24\n" +
            "FROM \"Customer Hub - Revenue Analysis\"\n" +
            "WHERE\n" +
            "((\"Accounting Date\".\"Accounting Month\" BETWEEN '@startYear / @startMonth' AND '@endYear / @endMonth') AND (\"Finance Line of Business\".\"Finance LOB\" IN ('Media Dealers', 'Software', 'Unspecified', 'Wholesale')) AND (\"GL Account\".\"Segment 3 Code\" IN ('40000', '40100', '45000')) AND (\"Subscriber Customer\".\"Customer Type\" = 'DEALER'))\n" +
            "ORDER BY 1, 2 ASC NULLS LAST, 8 ASC NULLS LAST, 7 ASC NULLS LAST, 9 ASC NULLS LAST, 13 ASC NULLS LAST, 17 ASC NULLS LAST, 23 ASC NULLS LAST, 3 ASC NULLS LAST, 18 ASC NULLS LAST, 14 ASC NULLS LAST, 4 ASC NULLS LAST, 15 ASC NULLS LAST, 19 ASC NULLS LAST, 20 ASC NULLS LAST, 21 ASC NULLS LAST, 12 ASC NULLS LAST, 22 ASC NULLS LAST, 24 ASC NULLS LAST, 10 ASC NULLS LAST, 11 ASC NULLS LAST, 6 ASC NULLS LAST, 5 ASC NULLS LAST, 16 ASC NULLS LAST\n" +
            "FETCH FIRST 10000001 ROWS ONLY\n";

    public static final String HEADER_PULL_QUARTERLY_REVENUE_ALL = "0,Accounting Month,Account Number,Account Type Code,Customer Number,Customer Subtype,Business Unit,Finance LOB,RMB LOB Code,Segment 3 Code,Segment 3 Name,Customer Franchise Indicator,Customer Name,Customer Operational Role,Customer Sales View Flag,Customer Status,Customer SVOC ID,Customer Type,Dealer Group Flag,Dealer Group ID Level 1,Dealer Group Name Level 1,DMA Code,Primary Cust  SVOC ID,Region ID,Revenue Amount";




    public static final String QueryName_PRODUCT_MONTHLY_EXTRACT = "product_monthly_extract";

    public static final String PRODUCT_MONTHLY_EXTRACT = "SELECT\n" +
            "   0 s_0,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Accounting Date\".\"Accounting Month\" s_1,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Business Unit\" s_2,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Finance LOB\" s_3,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"RMB LOB Description\" s_4,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Business Unit\" s_5,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Charge Type Code\" s_6,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Charge Type Description\" s_7,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Co-op Product Description\" s_8,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Deferred Flag\" s_9,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Deferred Revenue Account\" s_10,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Department Code\" s_11,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"GL Product Code\" s_12,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"GM Product Code\" s_13,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Invoice Product Description\" s_14,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Location\" s_15,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Package for Charge Type\" s_16,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Product Category\" s_17,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Product Code\" s_18,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Product Family\" s_19,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Product Taxability Flag\" s_20,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Production Cost Flag\" s_21,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Proratable Flag\" s_22,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Revenue Affilate Account\" s_23,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Revenue Natural Account\" s_24,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Product\".\"Usage Product\" s_25,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Revenue Details\".\"Contract End Date\" s_26,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Revenue Details\".\"Contract ID\" s_27,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Revenue Details\".\"Contract Start Date\" s_28,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Revenue Details\".\"Contract Type\" s_29,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Revenue Details\".\"Description on Bill\" s_30,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Revenue Details\".\"Recurring Flag\" s_31,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Name\" s_32,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Status\" s_33,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer SVOC ID\" s_34,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Type\" s_35\n" +
            "FROM \"Customer Hub - Revenue Analysis\"\n" +
            "WHERE\n" +
            "(\"Subscriber Customer\".\"Customer Type\" = 'DEALER')\n" +
            "ORDER BY 1, 2 ASC NULLS LAST, 3 ASC NULLS LAST, 4 ASC NULLS LAST, 5 ASC NULLS LAST, 6 ASC NULLS LAST, 7 ASC NULLS LAST, 8 ASC NULLS LAST, 9 ASC NULLS LAST, 10 ASC NULLS LAST, 11 ASC NULLS LAST, 12 ASC NULLS LAST, 13 ASC NULLS LAST, 14 ASC NULLS LAST, 15 ASC NULLS LAST, 16 ASC NULLS LAST, 17 ASC NULLS LAST, 19 ASC NULLS LAST, 20 ASC NULLS LAST, 21 ASC NULLS LAST, 22 ASC NULLS LAST, 23 ASC NULLS LAST, 24 ASC NULLS LAST, 25 ASC NULLS LAST, 26 ASC NULLS LAST, 18 ASC NULLS LAST, 28 ASC NULLS LAST, 29 ASC NULLS LAST, 27 ASC NULLS LAST, 30 ASC NULLS LAST, 31 ASC NULLS LAST, 32 ASC NULLS LAST, 33 ASC NULLS LAST, 35 ASC NULLS LAST, 34 ASC NULLS LAST, 36 ASC NULLS LAST\n" +
            "FETCH FIRST 10000001 ROWS ONLY\n";

    public static final String HEADER_PRODUCT_MONTHLY_EXTRACT = "0,Accounting Month,Business Unit,Finance LOB,RMB LOB Description,Business Unit,Charge Type Code,Charge Type Description,Co-op Product Description,Deferred Flag,Deferred Revenue Account,Department Code," +
            "GL Product Code,GM Product Code,Invoice Product Description,Location,Package for Charge Type,Product Category,Product Code,Product Family,Product Taxability Flag,Production Cost Flag,Proratable Flag,Revenue Affilate Account," +
            "Revenue Natural Account,Usage Product,Contract End Date,Contract ID,Contract Start Date,Contract Type,Description on Bill,Recurring Flag,Customer Name,Customer Status,Customer SVOC ID,Customer Type";



    public static final String QueryName_MARKETING_RMB_MONTHLY_EXTRACT = "marketing_rmb_monthly_extract";

    public static final String MARKETING_RMB_MONTHLY_EXTRACT = "SELECT\n" +
            "   0 s_0,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Accounting Date\".\"Accounting Year\" s_1,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer City Name\" s_2,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Franchise Independent Flag\" s_3,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Name\" s_4,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Number\" s_5,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Postal Code\" s_6,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer State Name\" s_7,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Street Address 1\" s_8,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Street Address 2\" s_9,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Street Address 3\" s_10,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Business Unit\" s_11,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Contact Name\" s_12,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Name\" s_13,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Operational Role\" s_14,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Status\" s_15,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer SVOC ID\" s_16,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Email Address\" s_17,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Lot Size (New)\" s_18,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Lot Size (Used)\" s_19,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Main Phone Number\" s_20,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Revenue\".\"Avg Revenue Amount\" s_21,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Revenue\".\"Revenue Amount YTD\" s_22,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Revenue\".\"Revenue Amount\" s_23\n" +
            "FROM \"Customer Hub - Revenue Analysis\"\n" +
            "WHERE\n" +
            "(\"Cox Automotive Customers\".\"Customer Franchise Independent Flag\" IN ('FRANCHISE', 'INDEPENDENT'))\n" +
            "ORDER BY 1, 12 ASC NULLS LAST, 2 ASC NULLS LAST, 4 ASC NULLS LAST, 6 ASC NULLS LAST, 5 ASC NULLS LAST, 9 ASC NULLS LAST, 10 ASC NULLS LAST, 11 ASC NULLS LAST, 3 ASC NULLS LAST, 8 ASC NULLS LAST, 7 ASC NULLS LAST, 16 ASC NULLS LAST, 17 ASC NULLS LAST, 19 ASC NULLS LAST, 20 ASC NULLS LAST, 21 ASC NULLS LAST, 18 ASC NULLS LAST, 13 ASC NULLS LAST, 15 ASC NULLS LAST, 14 ASC NULLS LAST\n" +
            "FETCH FIRST 10000001 ROWS ONLY\n";

    public static final String HEADER_MARKETING_RMB_MONTHLY_EXTRACT = "0,Accounting Year,Customer City Name,Customer Franchise Independent Flag,Customer Name,Customer Number,Customer Postal Code,Customer State Name," +
            "Customer Street Address 1,Customer Street Address 2,Customer Street Address 3,Business Unit,Contact Name,Customer Name,Customer Operational Role,Customer Status,Customer SVOC ID,Email Address,Lot Size (New)," +
            "Lot Size (Used),Main Phone Number,Avg Revenue Amount,Revenue Amount YTD,Revenue Amount";



    public static final String QueryName_MANHEIM_QUARTERLY_EXTRACT_BUY_SELL = "manheim_quarterly_extract_buy_sell";

    public static final String MANHEIM_QUARTERLY_EXTRACT_BUY_SELL = "SELECT\n" +
            "   0 s_0,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Accounting Date\".\"Accounting Month\" s_1,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Cox Automotive Customers\".\"Customer Number\" s_2,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Business Unit\" s_3,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Financial Business Unit\" s_4,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy  Address City\" s_5,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy  Address ZIP\" s_6,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy Address State\" s_7,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy Street Address 1\" s_8,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Franchise Indicator\" s_9,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Name\" s_10,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Sales View Flag\" s_11,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Status\" s_12,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer SVOC ID\" s_13,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Type\" s_14,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Name Plates\" s_15,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Primary Cust  SVOC ID\" s_16,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Manheim Measures\".\"Buy Amount\" s_17,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Manheim Measures\".\"Buy Units\" s_18,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Manheim Measures\".\"Sell Amount\" s_19,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Manheim Measures\".\"Sell Units\" s_20,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Revenue\".\"Revenue Amount >0\" s_21\n" +
            "FROM \"Customer Hub - Revenue Analysis\"\n" +
            "WHERE\n" +
            "((\"Accounting Date\".\"Accounting Month\" BETWEEN '@startYear / @startMonth' AND '@endYear / @endMonth') AND (\"Finance Line of Business\".\"Business Unit\" = 'MAN'))\n" +
            "ORDER BY 1, 2 ASC NULLS LAST, 4 ASC NULLS LAST, 5 ASC NULLS LAST, 13 ASC NULLS LAST, 17 ASC NULLS LAST, 15 ASC NULLS LAST, 11 ASC NULLS LAST, 14 ASC NULLS LAST, 10 ASC NULLS LAST, 9 ASC NULLS LAST, 6 ASC NULLS LAST, 8 ASC NULLS LAST, 7 ASC NULLS LAST, 12 ASC NULLS LAST, 16 ASC NULLS LAST, 3 ASC NULLS LAST\n" +
            "FETCH FIRST 10000001 ROWS ONLY\n";

    public static final String HEADER_MANHEIM_QUARTERLY_EXTRACT_BUY_SELL = "0,Accounting Month,Customer Number,Business Unit,Financial Business Unit,Cust Phy  Address City,Cust Phy  Address ZIP,Cust Phy Address State," +
            "Cust Phy Street Address 1,Customer Franchise Indicator,Customer Name,Customer Sales View Flag,Customer Status,Customer SVOC ID,Customer Type,Name Plates,Primary Cust  SVOC ID,Buy Amount,Buy Units," +
            "Sell Amount,Sell Units,Revenue Amount";




    public static final String QueryName_FRANCHISE_NAMEPLATES_EXTRACT = "franchise_nameplates_extract";

    public static final String FRANCHISE_NAMEPLATES_EXTRACT = "SELECT\n" +
            "   0 s_0,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Business Unit\" s_1,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Finance LOB\" s_2,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Finance Line of Business\".\"Financial Business Unit\" s_3,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy  Address City\" s_4,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy Address Country\" s_5,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy Address State\" s_6,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy Street Address 1\" s_7,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Cust Phy Street Address 2\" s_8,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Franchise Indicator\" s_9,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Name\" s_10,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Operational Role\" s_11,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Status\" s_12,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Sub Type\" s_13,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer SVOC ID\" s_14,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Customer Type\" s_15,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"District ID\" s_16,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"DMA\" s_17,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Lot Size (New)\" s_18,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Lot Size (Used)\" s_19,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Lot Size Sorted (New)\" s_20,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Lot Size Sorted (Used)\" s_21,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Name Plates\" s_22,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Physical Address Valid Flag\" s_23,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Subscriber Customer\".\"Region ID\" s_24,\n" +
            "   \"Customer Hub - Revenue Analysis\".\"Fact - Revenue\".\"Primary Subscriber Count\" s_25\n" +
            "FROM \"Customer Hub - Revenue Analysis\"\n" +
            "WHERE\n" +
            "((\"Subscriber Customer\".\"Customer Type\" = 'DEALER') AND (\"Subscriber Customer\".\"Customer Franchise Indicator\" = 'FRANCHISE'))\n" +
            "ORDER BY 1, 16 ASC NULLS LAST, 11 ASC NULLS LAST, 12 ASC NULLS LAST, 13 ASC NULLS LAST, 14 ASC NULLS LAST, 15 ASC NULLS LAST, 18 ASC NULLS LAST, 10 ASC NULLS LAST, 19 ASC NULLS LAST, 20 ASC NULLS LAST, 21 ASC NULLS LAST, 22 ASC NULLS LAST, 23 ASC NULLS LAST, 24 ASC NULLS LAST, 5 ASC NULLS LAST, 6 ASC NULLS LAST, 7 ASC NULLS LAST, 8 ASC NULLS LAST, 9 ASC NULLS LAST, 25 ASC NULLS LAST, 17 ASC NULLS LAST, 2 ASC NULLS LAST, 4 ASC NULLS LAST, 3 ASC NULLS LAST\n" +
            "FETCH FIRST 10000001 ROWS ONLY\n";

    public static final String HEAEDR_FRANCHISE_NAMEPLATES_EXTRACT = "0,Business Unit,Finance LOB,Financial Business Unit,Cust Phy  Address City,Cust Phy Address Country,Cust Phy Address State,Cust Phy Street Address 1," +
            "Cust Phy Street Address 2,Customer Franchise Indicator,Customer Name,Customer Operational Role,Customer Status,Customer Sub Type,Customer SVOC ID,Customer Type,District ID,DMA,Lot Size (New),Lot Size (Used)," +
            "Lot Size Sorted (New),Lot Size Sorted (Used),Name Plates,Physical Address Valid Flag,Region ID,Primary Subscriber Count";


    public static final String QueryName_AR_AGING_REPORT = "ar_aging_report";

    public static final String AR_AGING_REPORT = "SELECT LOV.atg_bu AS BU,\n" +
            "lov.val AS LOB_CODE,\n" +
            "LOV.NAME AS LOB_DESC,\n" +
            "CA.X_CIS_DIVISION AS CIS_DIVISION, \n" +
            "(CASE WHEN PO.X_PARTY_SUBROLE IS NULL THEN PO.X_PARTY_ROLE ELSE (PO.X_PARTY_ROLE||'-'||PO.X_PARTY_SUBROLE) END) AS CUSTOMER_CLASS, \n" +
            "PO.X_LOC_REGION AS REGION, \n" +
            "REPLACE (CA.X_DISTRICT, '''', '') AS DISTRICT,\n" +
            "PO.cust_status_code AS CUST_STATUS_CODE,\n" +
            "PO.CUSTOMER_NUM AS BILLING_SVOC_ID,\n" +
            "PO.org_name AS BILLING_NAME,\n" +
            "CA.X_BILLING_ACCOUNT_ID AS BILLING_ACCOUNT_ID,\n" +
            "PO.X_PSR_FULL_NAME AS SALENAME,\n" +
            "CA.X_PAY_TERM AS PAY_TERM,\n" +
            "CA.X_LAST_PAY_AMT AS LAST_PAY_AMT,\n" +
            "CA.X_LAST_PAY_DATE AS LAST_PAY_DATE,\n" +
            "CA.LOC_CURR_CODE AS CURRENCY_CD,\n" +
            "CA.X_TOTAL_AMT AS TOTAL_AMT,\n" +
            "CA.ODUE_1_LOC_AMT AS BUCKET_0_30,\n" +
            "CA.ODUE_2_LOC_AMT AS BUCKET_31_60,\n" +
            "CA.ODUE_3_LOC_AMT AS BUCKET_61_90,\n" +
            "CA.ODUE_4_LOC_AMT AS BUCKET_91_120,\n" +
            "CA.X_ODUE_5_LOC_AMT AS BUCKET_FT_121,\n" +
            "SYSDATE as RUN_DATE\n" +
            "FROM W_AR_AGING_CUSTOMER_A CA, \n" +
            "WC_LOV_D LOV,\n" +
            "W_PARTY_ORG_D PO\n" +
            "WHERE 1=1\n" +
            "AND CA.X_LOV_D_WID = LOV.ROW_WID\n" +
            "AND CA.X_PARTY_ORG_WID = PO.ROW_WID\n" +
            "AND CA.snapshot_dt_wid = (SELECT MAX(CAX.SNAPSHOT_DT_WID) FROM W_AR_AGING_CUSTOMER_A CAX\n" +
            "WHERE CA.customer_wid = CAX.CUSTOMER_WID)\n" +
            "ORDER BY  LOV.atg_bu,\n" +
            "lov.val,\n" +
            "PO.CUSTOMER_NUM,\n" +
            "CA.X_BILLING_ACCOUNT_ID\n" ;


    public static final String HEADER_AR_AGING_REPORT = "BU,LOB_CODE,LOB_DESC,CIS_DIVISION,CUSTOMER_CLASS,REGION,DISTRICT,CUST_STATUS_CODE," +
            "BILLING_SVOC_ID,BILLING_NAME,BILLING_ACCOUNT_ID,SALENAME,PAY_TERM,LAST_PAY_AMT,LAST_PAY_DATE,CURRENCY_CD,TOTAL_AMT,BUCKET_0_30," +
            "BUCKET_31_60,BUCKET_61_90,BUCKET_91_120,BUCKET_FT_121,RUN_DATE";


    //public static final String result = "<rowset xmlns=\"urn:schemas-microsoft-com:xml-analysis:rowset\"><Row><Column0>0</Column0><Column1>2015 / 10</Column1><Column2>69049275</Column2><Column3>SVOC</Column3><Column4>CA10833851</Column4><Column5>RETAIL</Column5><Column6>ATC</Column6><Column7>Media Dealers</Column7><Column8>ATC-ATX</Column8><Column9>40000</Column9><Column10>Subscription</Column10><Column11>INDEPENDENT</Column11><Column12>400Wheels LLC</Column12><Column13>PRIMARY</Column13><Column14>Y</Column14><Column15>IN BUSINESS</Column15><Column16>69049275</Column16><Column17>DEALER</Column17><Column18>N</Column18><Column19>69049275</Column19><Column20>400Wheels LLC</Column20><Column21>ATLANTA</Column21><Column22>69049275</Column22><Column23>39</Column23><Column24>255</Column24></Row><Row><Column0>0</Column0><Column1>2015 / 10</Column1><Column2>64100665</Column2><Column3>SVOC</Column3><Column4>CA10914202</Column4><Column5>RETAIL</Column5><Column6>ATC</Column6><Column7>Media Dealers</Column7><Column8>ATC-ATX</Column8><Column9>40000</Column9><Column10>Subscription</Column10><Column11>INDEPENDENT</Column11><Column12>500 Classic Auto</Column12><Column13>PRIMARY</Column13><Column14>N</Column14><Column15>IN BUSINESS</Column15><Column16>64100665</Column16><Column17>DEALER</Column17><Column18>N</Column18><Column19>64100665</Column19><Column20>500 Classic Auto</Column20><Column21>INDIANAPOLIS</Column21><Column22>64100665</Column22><Column23>04</Column23><Column24>249</Column24></Row><Row><Column0>0</Column0><Column1>2015 / 10</Column1><Column2>68630249</Column2><Column3>SVOC</Column3><Column4>CA10930182</Column4><Column5>RETAIL</Column5><Column6>ATC</Column6><Column7>Media Dealers</Column7><Column8>ATC-ATX</Column8><Column9>40000</Column9><Column10>Subscription</Column10><Column11>INDEPENDENT</Column11><Column12>74 Auto</Column12><Column13>PRIMARY</Column13><Column14>Y</Column14><Column15>IN BUSINESS</Column15><Column16>68630249</Column16><Column17>DEALER</Column17><Column18>N</Column18><Column19>68630249</Column19><Column20>74 Auto</Column20><Column21>PADUCAH-CGIRD-HARBG-MT_VN</Column21><Column22>68630249</Column22><Column23>39</Column23><Column24>100</Column24></Row></rowset>";
}
