package com.coxautoinc.eds.ingestion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;

import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;


/**
 * Created by akgillella on 7/27/16.
 */
public class ObieeQueryResultParser  extends DefaultHandler {

    private static final Logger logger = LoggerFactory.getLogger(ObieeQueryResultParser.class);

    private String outputFileName;

    private String reportHeader;
    boolean isColumn = false;
    StringBuffer sb ;
    int recordCount = 0;
    FileWriter fw;


    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    public void setReportHeader(String reportHeader) {
        this.reportHeader = reportHeader;
    }

    @Override
    public void startDocument() throws SAXException {

        try{

            if(outputFileName != null && !outputFileName.isEmpty()) {
                File file = new File(outputFileName);
                if(file.exists()){
                    logger.warn("old output file exist...moving it with .old extension");

                    Files.move(FileSystems.getDefault().getPath(outputFileName), FileSystems.getDefault().getPath(outputFileName+".old"), StandardCopyOption.REPLACE_EXISTING);
                }

                fw = new FileWriter(outputFileName,true);
            }else {
                logger.warn("Please set outputFileName to store results");
                logger.info("Since output file name is not set....Setting it to a '/tmp/tmpOutputFile.csv'");
                outputFileName = "/tmp/tmpOutputFile.csv";

                File file = new File(outputFileName);
                if(file.exists()){
                    logger.warn("old output file: {} exist...moving it with .old extension", outputFileName);

                    Files.move(FileSystems.getDefault().getPath(outputFileName), FileSystems.getDefault().getPath(outputFileName+".old"), StandardCopyOption.REPLACE_EXISTING);
                }

            }

            if(reportHeader != null && !reportHeader.isEmpty()) {
                fw.write(reportHeader+"\n");
             }
        }catch(Exception e){
                e.printStackTrace();
        }
    }

    @Override
    public void endDocument() throws SAXException {

        try{
            fw.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws SAXException {
        if(qName.startsWith("Column")){
            isColumn = true;
        }

        if(qName.startsWith("Row")){
            isColumn = false;
            sb = new StringBuffer();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        try{
            if(qName.startsWith("Row")){

                sb.trimToSize();
                sb.deleteCharAt(sb.length()-1);
                logger.debug("writing record to csv: {}", sb.toString()+"\n");
                fw.write(sb.toString()+"\n");
                recordCount++;
            }

            if(recordCount >= 1000){
                fw.flush();
                recordCount=0;
            }


        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(isColumn){
            sb.append(ch, start, length);
            sb.append(",");
        }
    }
}
